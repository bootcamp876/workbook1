﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
//using AnimalShelter.Data;
using AnimalShelter.Models;
using AnimalShelter;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<AnimalShelterContext>(options =>
   options.UseSqlServer(builder.Configuration.GetConnectionString("AnimalShelterContext") ?? throw new InvalidOperationException("Connection string 'AnimalShelterContext' not found.")));

//builder.Services.AddDbContext<AnimalShelterContext>(options => options.UseSqlServer(
// builder.Configuration.GetConnectionString(
//"Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=AnimalShelter;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False")));
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

//Add services to the container.
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins("http://localhost:4200",
                                              "http://www.contoso.com").AllowAnyHeader().AllowAnyMethod();
                      });
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors(MyAllowSpecificOrigins);

app.UseAuthorization();

app.MapControllers();

app.Run();
