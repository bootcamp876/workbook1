﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AnimalShelter;
using AnimalShelter.Models;

namespace AnimalShelter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalsController : ControllerBase
    {
        private readonly AnimalShelterContext _context;

        public AnimalsController(AnimalShelterContext context)
        {
            _context = context;
        }

        // GET: api/Animals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Animal>>> GetAnimals()
        {
          if (_context.Animals == null)
          {
              return NotFound();
          }
            return await _context.Animals.ToListAsync();
        }

        // GET: api/Animals/5
        [HttpGet("{AnimalId}")]
        public async Task<ActionResult<Animal>> GetAnimals(int AnimalId)
        {
          if (_context.Animals == null)
          {
              return NotFound();
          }
            var animal = await _context.Animals.FindAsync(AnimalId);

            if (animal == null)
            {
                return NotFound();
            }

            return animal;
        }

        // POST: api/Animals
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Animal>> PostAnimal(Animal animal)
        {
            if (_context.Animals == null)
            {
                return Problem("Entity set 'AnimalShelterContext.Animals'  is null.");
            }
            _context.Animals.Add(animal);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAnimal", new { AnimalId = animal.AnimalId }, animal);
        }

        // PUT: api/Animals/5       
        [HttpPut("{AnimalId}")]
        public async Task<IActionResult> PutAnimal(int AnimalId, Animal animal)
        {
            if (AnimalId != animal.AnimalId)
            {
                return BadRequest();
            }

            _context.Entry(animal).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimalExists(AnimalId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Animals/5
        [HttpDelete("{AnimalId}")]
        public async Task<IActionResult> DeleteAnimal(int AnimalId)
        {
            if (_context.Animals == null)
            {
                return NotFound();
            }
            var animal = await _context.Animals.FindAsync(AnimalId);
            if (animal == null)
            {
                return NotFound();
            }

            _context.Animals.Remove(animal);
            await _context.SaveChangesAsync();

            return NoContent();
        }
      
        [Route("{owner}/owner")]
        [HttpGet]
        public ActionResult GetByOwner(string owner)
        {
            if (_context.Animals == null)
            {
                return NotFound();
            }

            var animal = _context.Animals.Where(a => a.Owner == owner);
            if (animal == null)
            {
                return NotFound();
            }

            return Ok(animal);
        }

        private bool AnimalExists(int AnimalId)
        {
            return (_context.Animals?.Any(e => e.AnimalId == AnimalId)).GetValueOrDefault();
        }
    }
}

