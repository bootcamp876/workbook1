﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AnimalShelter.Models;

namespace AnimalShelter
{
    public class AnimalShelterContext : DbContext
    {
        public AnimalShelterContext()
        {

        }

    public AnimalShelterContext(DbContextOptions<AnimalShelterContext> options)
        : base(options)
        {
        }

        public DbSet<Animal> Animals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=AnimalShelter;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>(entity =>
            {
                entity.HasKey(e => e.AnimalId).HasName("PK__AnimalSh__A21A7307921CAD03");

                entity.Property(e => e.AnimalId).ValueGeneratedNever();
                entity.Property(e => e.Breed).HasMaxLength(50);
                entity.Property(e => e.CheckInDate).HasColumnType("datetime");
                entity.Property(e => e.CheckOutDate).HasColumnType("datetime");
                entity.Property(e => e.Name).HasMaxLength(50);
                entity.Property(e => e.Owner).HasMaxLength(50);
                entity.Property(e => e.Species).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        public void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {

        }
    }
}

