﻿using AnimalShelter.Models;
using Microsoft.AspNetCore.Mvc;

namespace AnimalShelter.Models
{ 

    [Route("api/animals")]
    [ApiController]
    public class AnimalManager
    {
        /*
        //GET: api/Animals
        [HttpGet]
        public IEnumerable<Animal> GetAll()
        {
            List<Animal> animals = null;
            using (var context = new AnimalShelterContext())
            {
                animals = context.Animals.ToList();
            }
            return animals;
        }

        //GET: api/Animals/AnimalId
        [HttpGet("{AnimalId}")]
        public Animal GetById(int AnimalId)
        {
            Animal animal = null;
            using (var context = new AnimalShelterContext())
            {
                animal = context.Animals
                                .Where(c => c.AnimalId == AnimalId)
                                .SingleOrDefault();
            }

            return animal;
        }

        //POST: api/Animals/
        [HttpPost]
        public void Add(Animal animal)
        {
            using (var context = new AnimalShelterContext())
            {
                context.Animals.Add(animal);
                context.SaveChanges();
            }
        }

        [HttpPut]
        public void Update(Animal animal)
        {
            using (var context = new AnimalShelterContext())
            {
                context.Animals.Update(animal);
                context.SaveChanges();
            }
        }

        [HttpDelete]
        public void Delete(Animal animal)
        {
            using (var context = new AnimalShelterContext())
            {
                context.Animals.Remove(animal);
                context.SaveChanges();
            }
        }

      
        [HttpGet("{owner}/owner")]
        public void FindByOwner(string owner)
        {
            using (var context = new AnimalShelterContext())
            {
                var result = context.Animals.Get(owner);
            }
        } 
      */
    }
}
