﻿using System;

namespace ClassPlay
{
    internal class ClassPlayProgram
    {

        static void Main(string[] args)
        {
            Employee employee1 = new Employee();
            Employee employee2 = new Employee("Bob", "Jones", "45");
            Employee employee3 = new Employee("James", "Brown", "46", "1956", "Singer", "Motown", "1000000");
            
            DisplayEmployee(employee1);
            DisplayEmployee(employee2);
            DisplayEmployee(employee3);
            employee2.Promote("CEO", "250000");
            DisplayEmployee(employee2);

            //employee.FirstName = "Toni";
            //employee.LastName = "Whitfield";
            //employee.EmployeeId = 25;
            //employee.YearJoined = 2023;
            //employee.JobTitle = "Clerk";
            //employee.Department = "Shipping";
            //employee.Salary = 30000;         

        }
      
        static void DisplayEmployee(Employee employee)
        {
            //Console.WriteLine("Employee First Name: " + employee.FirstName);
            //Console.WriteLine("Employee Last Name: " + employee.LastName);
            //Console.WriteLine("Employee Id: " + employee.EmployeeId);
            //Console.WriteLine("Employee hire year: " + employee.YearJoined);
            //Console.WriteLine("Employee Job Title: " + employee.IobTitle);
            //Console.WriteLine("Employee Department: " + employee.Department);
            //Console.WriteLine("Employee Salary: " + employee.Salary);
            
            Console.WriteLine("Name: {0} {1}", employee.FirstName, employee.LastName);
            Console.WriteLine("Employee ID: {0}",  employee.EmployeeId);
            Console.WriteLine("Year Joined: {0} ", employee.YearJoined);
            Console.WriteLine("Title: {0} ", employee.JobTitle);
            Console.WriteLine("Department: {0}", employee.Department);
            Console.WriteLine("Salary: {0}", employee.Salary);

        }
    }
}
