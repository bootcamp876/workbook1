﻿using System;

namespace ClassPlay
{
    public class Employee
    {
        //These fields are properties since they have get; set;
        //get and set are methods

        //Properties
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string EmployeeId { get; set; } = "0";
        public string YearJoined { get; set; } = "0";
        public string JobTitle { get; set; } = "";
        public string Department { get; set; } = "";
        public string Salary { get; set; } = "0";

        /*this.FirstName = firstName;
        this.LastName = lastName;
        this.EmployeeId = employeeId;
        this.Year = year;
        this.JobTitle = jobTitle;
        this.Department = department;
        this.Salary = salary;*/

        //Constructor
        public Employee(string firstName, string lastName, string employeeId, string year, string jobTitle, string department, string salary)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.EmployeeId = employeeId;
            this.YearJoined = year;
            this.JobTitle = jobTitle;
            this.Department = department;
            this.Salary = salary;
        }

        public Employee(string firstName, string lastName, string employeeId)
        {
            DateTime now = DateTime.Now;

            this.FirstName = firstName;
            this.LastName = lastName;
            this.EmployeeId = employeeId;
            this.YearJoined = now.ToString("yyyy");
            this.JobTitle = "New Hire";
            this.Department = "TBD";
            this.Salary = "31200";
        }

        public Employee() : this("Toni", "Whitfield", "25")
        {

        }

        public void DisplayEmployee()
        {
            Console.WriteLine("Name: {0} {1}", FirstName, LastName);
            Console.WriteLine("Employee ID: {0}", EmployeeId);
            Console.WriteLine("Year Joined: {0} ", YearJoined);
            Console.WriteLine("Title: {0} ", JobTitle);
            Console.WriteLine("Department: {0}", Department);
            Console.WriteLine("Salary: {0}", Salary);
        }

        public void Promote(string newJobTitle, string newPay)
        {
            JobTitle = newJobTitle;
            Salary = newPay;
        }

    }
}


