﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrubbing
{
    public class DataScrubber
    {
        public static void ScrubPhone(ref string phoneNumber)
        {
            phoneNumber = phoneNumber.Trim();
        }

        public static void ScrubPhone(ref string phoneNumber, string charToRemove)
        {
            phoneNumber = phoneNumber.Trim();
            phoneNumber = phoneNumber.Replace(charToRemove, "");
            
        }

        public static void ScrubPhone(ref string phoneNumber, string charToRemove, string charToReplaceWith)
        {
            phoneNumber = phoneNumber.Trim();
            phoneNumber = phoneNumber.Replace(charToRemove, charToReplaceWith);
        }
    }
}
