﻿using DataScrubbing;

string phoneNumber = "";
string another = "N";
string answer = "";
string charToRemove = "";
string charToReplace = "";

do
{
    Console.WriteLine("Scrubbing Methods");
    Console.WriteLine("1. Trim Characters");
    Console.WriteLine("2. Remove Characters");
    Console.WriteLine("3. Replace Characters");
    answer = Console.ReadLine();

    if (answer == "1")
    {
        Console.Write("Enter a Phone Number ");
        phoneNumber = Console.ReadLine();
        DataScrubber.ScrubPhone(ref phoneNumber);

        Console.WriteLine($"Scrubbed Phone Number: {phoneNumber}");
    }
    else if (answer == "2")
    {
        Console.Write("Enter a Phone Number ");
        phoneNumber = Console.ReadLine();

        Console.Write("What character should be removed? ");
        charToRemove = Console.ReadLine();

        DataScrubber.ScrubPhone(ref phoneNumber, charToRemove);

        Console.WriteLine($"Scrubbed Phone Number: {phoneNumber}");
    }
    else if (answer == "3")
    {
        Console.Write("Enter a Phone Number ");
        phoneNumber = Console.ReadLine();

        Console.Write("What character should be removed? ");
        charToRemove = Console.ReadLine();

        Console.Write("What should it be replaced with? ");
        charToReplace = Console.ReadLine();

        DataScrubber.ScrubPhone(ref phoneNumber, charToRemove, charToReplace);

        Console.WriteLine($"Scrubbed Phone Number: {phoneNumber}");
    }
    else if (another == "N")
    {
        break;
    }

    

    Console.WriteLine("Would you like to enter another Phone Number? ");
    another = Console.ReadLine();

} while (another == "Y");
