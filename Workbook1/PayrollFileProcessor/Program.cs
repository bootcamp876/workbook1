﻿using System;
using System.IO;
using System.Text;

// In this example, we are not worried about a 
// any exceptions trying to open the file

public class Program
{
    static void Main(string[] args)
    {
        double totalGross = 0;
        string fileName = @"C:\Academy\Payroll1.txt";
        //string path = @"C:\Academy\Payroll1.txt";
        string path = @"C:\Academy\PayrollFileProcesser" + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".log";

        StreamReader? inputFile = null;
       
        try
        {
            File.Create(path).Close();
            Console.WriteLine("File created");

            inputFile = new StreamReader(fileName);
            
            while (!inputFile.EndOfStream)
            {              
                string text = inputFile.ReadLine();

                string id = text.Substring(0,5);
                string name = text.Substring(5,22);
                name = name.Trim();

                double payRate = double.Parse(text.Substring(30,5));
                double hoursWorked = double.Parse(text.Substring(36,4));
               // Console.WriteLine(payRate);
               // Console.WriteLine(hoursWorked);

                double grossPay = 0;
                                
                if (hoursWorked > 40)
                {
                    grossPay = (40 * payRate) + ((hoursWorked - 40) * (payRate * 1.5));
                    totalGross = totalGross + grossPay;
                    
                }
                else
                {
                    grossPay = (40 * payRate);
                    totalGross = totalGross + grossPay;
                }

                Console.WriteLine($"Hours Worked: {hoursWorked}");
                Console.WriteLine($"Pay Rate: {payRate}");
                Console.WriteLine($"Gross Pay: {grossPay}");
                Console.WriteLine($"Total Gross: {totalGross}");

            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error opening file: {ex.Message}");
        }
        finally
        {
            if (inputFile != null) inputFile.Close();
        }   
    }
}



