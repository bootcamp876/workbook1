﻿using System;

public class Program
{     

    public static void Main(string[] args)
    {
        int[] nums = { 3, 6, 9, 12, 15, 17, 21 };

        double avgAmt = GetAverage(nums);
        Console.WriteLine("The average number is: " + avgAmt);

        int median = GetMedian(nums);
        Console.WriteLine("The median number is: " + median);

        double largestNumber = GetLargestValue(nums);
        Console.WriteLine("The largest number is: " + largestNumber);


        double smallestNumber = GetSmallestValue(nums);
        Console.WriteLine("The smallest number is: " + smallestNumber);
    }

    public static double GetAverage(int[] nums)
    {
        int total = 0;        

        for (int i = 0; i < nums.Length; i++)
        {
            total = total + nums[i];                      
        }
        double avgAmt = total / nums.Length;
        return avgAmt;
    }
    
    public static int GetMedian(int[] nums)
    {
        int size = nums.Length;
        int mid = size / 2;

        int median = (size % 2 != 0) ? (int)nums[mid] : ((int)nums[mid] + (int)nums[mid - 1]) / 2;

        return median;
    }

    public static double GetLargestValue(int[] nums)
    {
        double largestNumber = nums.Max();
        return largestNumber;
        
    }

    public static double GetSmallestValue(int[] nums)
    {
        double smallestNumber = nums.Min();
        return smallestNumber;
        
    }
}