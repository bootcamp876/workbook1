﻿public class Program
{
   public static void Main(string[] args)
    {
       Console.Write("Enter Insurance Policy Renewal date: ");
       string str = Console.ReadLine();

       DateTime dueDate = Convert.ToDateTime(str);

       DateTime mailByDate = dueDate.AddDays(10);
       Console.WriteLine($"Mail your payment by: {mailByDate}");

       DateTime cancelByDate = dueDate.AddMonths(1);
       Console.WriteLine($"Cancellation Date: {cancelByDate}");
    }
 }      
