﻿using System;

public class Program
{
    public static void Main(string[] args)
    {
        string itemOrdered = "";
        decimal? itemPrice = null;
        string orderMore = "";

        string[] items = { 
            "Sausage Breakfast Taco", 
            "Potato and Egg Breakfast Taco", 
            "Sausage and Egg Biscuit",
            "Bacon and Egg Biscuit", 
            "Pancakes"};

        decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };

        
        while ( orderMore!= "N")
        {
            itemPrice = null;

            Console.Write("What would you like to order? ");
            itemOrdered = Console.ReadLine();

            itemPrice = GetOrder(itemOrdered, prices, items);

            if (itemPrice.HasValue)
            {
                Console.WriteLine($"You ordered {itemOrdered} {itemPrice}");
            }
            else 
            {
                Console.WriteLine("Item not found");
            }

            Console.Write("Do you want order anything else (Y/N?) ");
            orderMore = Console.ReadLine().ToUpper();
        }
    }

    public static decimal? GetOrder(string itemOrdered, decimal[] prices, string[] items)
    {
        int search = -1;

        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == itemOrdered)
            {
                search = i; 
                break;
            }
        }
        if (search == -1)
        {
            return null;
        }
        else { return prices[search]; }
    } 
}