﻿using System;
using System.Net;
using System.Text;
using System.Data;

namespace CarDealership
{
    public class Program
    {
        public static string[,] CreateArray()
        {
            string[,] cars = new string[5, 6];
            return cars;
            
            //      0      1       2       3      4       5
            //  0 {"1","Toyota","Camry","Blue","45000","15000"},
            //  1 {"2","Honda","Civic", "Red","30000","12000"},
            //  2 {"3","Ford","Focus","White","55000","9000"},
            //  3 {"4","Chevrolet","Malibu","Black","20000","13000"},
            //  4 {"5","Nissan","Altima","Silver","35000","11000"}

        }

        static void Main(string[] args)
        {
            string[,] cars = CreateArray();
            string path = @"C:\Academy\cars.txt";           
            string choice = "";
            
            StreamReader? inputFile = null;

            try
            {
                int i = 0;
                inputFile = new StreamReader(path);

                while (inputFile.EndOfStream != true)
                {
                    string line = inputFile.ReadLine();
                    string[] carArray = line.Split(",");

                    for (int j = 0; j < carArray.Length; j++)
                    {
                        cars[i, j] = carArray[j];
                    }
                    i++;
                }

                if (inputFile != null) inputFile.Close(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening file: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }

            Console.WriteLine("Please select a vehicle: ");
            Console.WriteLine();
            Console.WriteLine("1. Toyota Camry");
            Console.WriteLine("2. Honda Civic");
            Console.WriteLine("3. Ford Focus");
            Console.WriteLine("4. Chevrolet Malibu");
            Console.WriteLine("5. Nissan Altima");
            choice = Console.ReadLine();

            DisplayVehicles(cars, choice);

        }

        static void DisplayVehicles(string[,] cars, string choice)
        {
            string purchaseCar = "";
            string anotherCar = "";

            Console.WriteLine(" ");
            Console.WriteLine("Displaying Vehicle Details");
            CarDetails(cars, choice);

            Console.WriteLine("Would you like to purchase this car? (Y/N) ");
            purchaseCar = Console.ReadLine().ToUpper();
            Console.WriteLine(" ");

            if (purchaseCar == "Y")
            {
                PurchaseVehicle(cars, choice);
            }
            else
            {
                Console.WriteLine(" ");
                Console.WriteLine("Would you like to select another car? (Y/N) ");
                anotherCar = Console.ReadLine().ToUpper();               
            }  
        }

        static void PurchaseVehicle(string[,] cars, string choice)
        {
            //Console.WriteLine("Purchase This Vehicle");

            CreateInvoice(cars, choice);
            CarDetails(cars, choice);
        }

        static void CreateInvoice(string[,] cars, string choice)
        {
            string invoiceFile = @"C:\Academy\vehicleInvoice.txt";
            StreamWriter outputFile = new StreamWriter(invoiceFile);

            string name = "";
            string address = "";
            string city = "";
            string state = "";
            string zip = "";
            string phone = "";

            Console.WriteLine("Customer Invoice");
            Console.WriteLine(" ");

            Console.WriteLine("Owner Name: ");
            name = Console.ReadLine();
            outputFile.WriteLine(name);

            Console.WriteLine("Address: ");
            address = Console.ReadLine();
            outputFile.WriteLine(address);

            Console.WriteLine("City: ");
            city = Console.ReadLine();
            outputFile.WriteLine(city);

            Console.WriteLine("State: ");
            state = Console.ReadLine();
            outputFile.WriteLine(state);
            
            Console.WriteLine("Zip: ");
            zip = Console.ReadLine();
            outputFile.WriteLine(zip);

            Console.WriteLine("Phone: ");
            phone = Console.ReadLine();
            outputFile.WriteLine(phone);

            outputFile.WriteLine(" ");

            //CarDetails(cars, choice);

            outputFile.Close();

        }

        static void CarDetails(string[,] cars, string choice)
        {
            Console.WriteLine("Car Details");
            Console.WriteLine(" ");

            for (int i = 0; i < cars.GetLength(0); i++)
            {
                if (cars[i, 0] == choice)
                {
                    for (int j = 0; j < cars.GetLength(1); j++)
                    {
                       Console.WriteLine(cars[i, j]);
                    }
                }
            }
            Console.WriteLine(" ");
        }
    }
}