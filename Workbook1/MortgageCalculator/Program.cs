﻿class Program
{
    static void Main(string[] args)
    {
        decimal borrow = 0.0M;
        decimal rate = 0.0M;
        decimal years = 0.0M;
        decimal payment = 0.0M;
        decimal totalPay = 0.0M;
        decimal totalInterest = 0.0M;
        float denominator = 0.0F;
        string anotherLoan = "";

        while (anotherLoan != "N")
        {
            borrow = 0;
            while (borrow <= 0)
            {
                Console.Write("How much are you borrowing? ");
                try
                {
                    borrow = Convert.ToDecimal(Console.ReadLine());
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Not a valid amount.  Enter a valid amount: " + e.Message);
                    borrow = 0;
                }
            }

            Console.Write("What is your interest rate? ");
            rate = Convert.ToDecimal(Console.ReadLine());

            Console.Write("How long is your loan (in years)? ");
            years = Convert.ToDecimal(Console.ReadLine());

            denominator = (float)Math.Pow(Convert.ToDouble(rate / 1200) + 1, -1 * Convert.ToDouble(years * 12));
            payment = borrow * (rate / 1200) / Convert.ToDecimal(1 - denominator);
            totalPay = (payment * 12) * years;
            totalInterest = totalPay - borrow;

            Console.WriteLine($"Your estimated payment is: {payment}");
            Console.WriteLine($"You paid {totalPay} over the life of the loan");
            Console.WriteLine($"Your total interest cost for the loan was {totalInterest}");

            Console.Write("Do you want to get information for another loan (Y/N? ");
            anotherLoan = Console.ReadLine().ToUpper();
        }
    }
}

