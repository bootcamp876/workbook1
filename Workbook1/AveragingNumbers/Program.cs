﻿using System;

//namespace workspace
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            decimal bands = 5;

//            if (bands < 1) {
//                Console.WriteLine("There will be no performances tonight.");
//            }
//            else if (bands > 0 && bands < 2) {
//                Console.WriteLine("It's going to be a fantastic show tonight!");
//            }
//            else
//            {
//                Console.WriteLine("There will be plenty of thrilling performances to see tonight!");
//            }
//        }
//    }      
//}
using System.Data.SqlClient;

namespace ExceptionsCourse
{
    public static partial class Example
    {
        public static void UpdateUser()
        {
            /* Start try block here */
            try
            {
                UpdateUserData();
            }
            /* Remaining code goes here */
            catch (SqlException)
            {
                Log.AlertEveryone(ex);
                throw;
            }
            catch (Exception ex)
            {
                ReportError(ex);
                Log.Error(ex);
                throw;
            }
            finally
            {
                DisposeOfDatabaseConnection();
            }
        }
    }
}