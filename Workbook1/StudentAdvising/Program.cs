﻿using System;

public class Program
{
    static void Main(string[] args)
    {
        string majorCode = "";
        string classification = "";
        string studentName = "";
        string advisingLocation = "";
        string anotherStudent = "";

        while (anotherStudent != "N")
        {

            GetData(out majorCode, out studentName, out classification);
            advisingLocation = GetAdvisingLocation(ref majorCode, classification);

            Console.WriteLine($"Advising for {studentName} {majorCode} {classification} majors: {advisingLocation} ");

            Console.Write("Do you want to enter another student (Y/N?) ");
            anotherStudent = Console.ReadLine().ToUpper();
        }
    }

    public static void GetData(out string majorCode, out string studentName, out string classification)
    {
        
        Console.Write("What is the student's name? ");
        studentName = Console.ReadLine();

        Console.Write("What is the student's major? ");
        majorCode = Console.ReadLine();

        Console.Write("What is the student's classification? ");
        classification = Console.ReadLine();  

    } //public 

    public static string GetAdvisingLocation(ref string majorCode, string classification)
    {
        string department = "";

        switch (majorCode)
        {
            case "BIOL":
                {
                   // majorCode = "BIOL";
                    majorCode = "Biology";

                    if (classification == "Freshman" | classification == "Sophomore")
                    {
                        department = "Science Bldg, Room 310";
                    }
                    else if (classification == "Junior" | classification == "Senior")
                    {
                        department = "Science Bldg, Room 311";
                    }

                   // Console.WriteLine($"Advising for {majorCode} {classification} majors: {department} ");
                    break;
                }
            case "CSCI":
                {
                    majorCode = "Computer Science";
                    department = "Sheppard Hall, Room 314";
                   // Console.WriteLine($"Advising for {majorCode} {classification} majors: {department} ");
                    break;
                }

            case "ENG":
                {
                    majorCode = "English";
                    if (classification == "Freshman")
                    {
                        department = "Kerr Hall, Room 201";
                    }

                    if (classification == "Sophomore" | classification == "Junior" | classification == "Senior")
                    {
                        department = "Kerr Hall, Room 312";
                    }
                   // Console.WriteLine($"Advising for {majorCode} {classification} majors: {department} ");
                    break;
                }
            case "HIST":
                {
                    majorCode = "History";
                    department = "Kerr Hall, Room 114";
                   // Console.WriteLine($"Advising for {majorCode} {classification} majors: {department} ");
                    break;
                }

            case "MKT":
                {
                    majorCode = "Marketing";
                    if (classification == "Freshman" | classification == "Sophomore" | classification == "Junior")
                    {
                        department = "Wesley Hall, Room 310";
                    }
                    if (classification == "Senior")
                    {
                        department = "Wesley Hall, Room 313";
                    }
                  //  Console.WriteLine($"Advising for {majorCode} {classification} majors: {department} ");
                    break;
                }

            default:
                {
                    Console.WriteLine($"{majorCode}) not found");
                    break;
                }
        } //switch
        return department;
    }
} //program

