﻿public class Program
{  
    public static int[,] CreateArray()
    {
        int[,] table = new int[10, 5];
        return table;
    }

    public static void Main()
    {
        int[,] mathTable = CreateArray();

        for (int i = 0; i < mathTable.GetLength(0); i++) // 10
        {       
            for (int j = 0; j < mathTable.GetLength(1); j++) // 5
            {
                mathTable[i, j] = i * j; // multiplication table
                Console.Write("{0, 6}", i * j);
            } 
            Console.WriteLine();
         }       
    }
}


