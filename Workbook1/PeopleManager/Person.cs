﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Person : IComparable<Person>   
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public Person(string name, int age, string address, string city, string state, string zipCode)
        {
            Name = name;
            Age = age;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipCode;
        }

        public int CompareTo(Person other)
        {
            return Name.CompareTo(other.Name);
        }

        public void HaveABirthay()
        {
            Console.WriteLine("Today is Wilma's Birthday!");
        }

        public void Move()
        {
            Console.WriteLine($"Please enter a new Street Address for {Name}: ");
            string Address = Console.ReadLine();

            Console.WriteLine("Please enter a new City: ");
            string City = Console.ReadLine();

            Console.WriteLine("Please enter a new State: ");
            string State = Console.ReadLine();

            Console.WriteLine("Please enter a new Zip Code: ");
            string ZipCode = Console.ReadLine();
        }

        public class AscendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return x.Age.CompareTo(y.Age);
            }
        }

        public virtual void Display()
        {
            Console.WriteLine($"{Name} is {Age}");
            Console.WriteLine("Address:");
            Console.WriteLine($"{Address}");
            Console.WriteLine($"{City},  {State}");
            Console.WriteLine($"{ZipCode}");
        }

        public class Worker : Person
        {
            public string JobTitle { get; set; }
            public decimal Salary { get; set; }

            public Worker(string name, int age, string address, string city, string state, string zipCode, string jobTitle, decimal salary) : base(name, age, address, city, state, zipCode)
            {
                JobTitle = jobTitle;
                Salary = salary;
            }

            public override void Display()
            {
                Console.WriteLine($"{Name} is {Age}");
                Console.WriteLine("Address:");
                Console.WriteLine($"{Address}");
                Console.WriteLine($"{City},  {State}");
                Console.WriteLine($"{ZipCode}");
                Console.WriteLine($"Job Title: {JobTitle}");
                Console.WriteLine($"Salary: {Salary}");
                Console.WriteLine(" ");
            }
        }

        public class DescendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                //Console.WriteLine("DescendingAgeSorter");
                return y.Age - x.Age;
            }
        }

        public class AscendingNameSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                //Console.WriteLine("AscendingNameSorter");
                return x.Name.CompareTo(y.Name);
            }
        }

        public class AscendingStateCitySorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                int compareInt = String.Compare(x.State, y.State, comparisonType: StringComparison.OrdinalIgnoreCase);
                if (compareInt != 0)
                {
                    // Console.WriteLine("AscendingStateCitySorter");
                    return String.Compare(x.City, y.City, comparisonType: StringComparison.OrdinalIgnoreCase);
                    //return compareInt;
                }
                 
                {
                    //return String.Compare(x.City, y.City, comparisonType: StringComparison.OrdinalIgnoreCase);
                    return compareInt;
                }
            }
        }

        public class DescendingZipSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                int per1 = Convert.ToInt32(x.ZipCode);
                int per2 = Convert.ToInt32(y.ZipCode);
                return per2 - per1;
            }
        }
    }
}
