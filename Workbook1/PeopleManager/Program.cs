﻿using System;
using System.Linq;
using System.Collections.Generic;
using PeopleManager;
using static PeopleManager.Person;

namespace PeopleManager
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Build the Person list
            List<Person> persons = new List<Person>();
            persons.Add(new Person("Fred Flintstone", 62, "123 Main St", "Detroit", "MI", "48220"));
            persons.Add(new Person("Wilma Flintstone", 59, "123 Main St", "Grosse Pointe Park", "MI", "48223"));
            persons.Add(new Person("Pebbles Flintstone", 1, "123 Main St", "Bedrock", "MI", "48221"));
            persons.Add(new Person("Barney Rubble", 60, " 321 Main St", "Royal Oak", "MI", "48221"));
            persons.Add(new Person("Betty Rubble", 56, "321 Main St", "Birmingham", "MI", "48218"));
            persons.Add(new Person("Bam Bam Rubble", 1, "321 Main St", "Bedrock", "MI", "48200"));
            persons.Add(new Person("Bill Gates", 68, "1 Microsoft Dr", "Malibu", "CA", "98735"));
            persons.Add(new Person("Warren Buffet", 90, "35 Billionaire Lane", "Highland Park", "IA", "27384"));
            persons.Add(new Person("Johnathon Scott", 45, "756 Property Brothers Lane", "Las Vegas", "NE", "37764"));

            //Console.WriteLine($"There are {persons.Count} elements in the list");
            //Console.WriteLine(" ");

            // persons[0].Move();
            // persons[2].HaveABirthay();

           // persons.Sort();
           // foreach (Person person in persons)
           //  {
           //    Console.WriteLine(person.Name);
           // }

           // persons.Sort(new AscendingStateCitySorter()); - not sure about this one
           // persons.Sort(new AscendingAgeSorter()); 
           // persons.Sort(new AscendingNameSorter());
           // persons.Sort(new DescendingAgeSorter());
           // persons.Sort(new DescendingZipSorter());

            foreach (Person person in persons)
            {
                person.Display();
            }
        }
    }
}
