﻿internal class Program
{
    static void Main(string[] args)
    {
        string answer = "";
        while (true)
        {
            Console.Write("Enter a Phone Number ");
            answer = (Console.ReadLine());

            if (answer == "CtoF")
            {
                // multiply by 1.8 and add 32
                GetFahrenheit();
            }
            else if (answer == "FtoC")
            {
                // subtract 32 and multiply by .5556
                GetCelsius();
            }
            else if (answer == "Quit")
            {
                break;
            }
        }
     }
    // method
    static double GetCelsius()
    {
        double fahrenheit = 0;
        Console.Write("Enter a Fahrenheit temperature: ");
        fahrenheit = Convert.ToInt32(Console.ReadLine());

        double celsius = (fahrenheit - 32) * .5556;
        Console.WriteLine($"{fahrenheit} degrees in fahrenheit is {celsius} degrees in Celsius");
        return celsius;      
    }
    // method
    static double GetFahrenheit()
    {
        int celsius = 0;
        Console.Write("Enter a Celsius temperature: ");
        celsius = Convert.ToInt32(Console.ReadLine());

        double fahrenheit = (celsius * 1.8) + 32;
        Console.WriteLine($"{celsius} degrees in Celsius is {fahrenheit} degrees in Fahrenheit");
        return fahrenheit;           
    }
}
