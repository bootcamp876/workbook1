﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    public abstract class Animal
    {
        public abstract string Name { get; set; }
        public abstract int Age { get; set; }
        public abstract string Sound { get; set; }

        //public Animal(string name, int age, string sound)
        //{
        //    this.Name = name;
        //    this.Age = age;
        //    this.Sound = sound;
        //}

        public abstract string MakeSound();

        public void DisplayInfo()
        {
            Console.WriteLine("Name: {0}, Age: {1}, Sound: {2}", this.Name, this.Age, this.Sound);
        }

    }
}
