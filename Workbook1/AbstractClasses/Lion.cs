﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    public class Lion : Animal, ICarnivore
    {
        public override string Name { get; set; }
        public override int Age { get; set; }
        public override string Sound { get; set; }

        public string Hunt()
        {
            return "The lion stalks its prey and pounces to catch it.";
        }

        public override string MakeSound()
        {
           return "Roar";
          // return this.Sound;

        }
    }
}
