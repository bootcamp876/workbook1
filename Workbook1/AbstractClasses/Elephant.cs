﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    public class Elephant : Animal, IHerbivore
    {
        public override string Name { get; set; }
        public override int Age { get; set; }
        public override string Sound { get; set; }

        public string Graze()
        {
            return "The elephant uses its trunk to pull leaves and branches from trees.";
        }

        public override string MakeSound()
        {
            //return this.Sound;
            return "Trumpet";
        }
    }
}
