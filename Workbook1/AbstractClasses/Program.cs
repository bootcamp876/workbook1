﻿using System;

namespace AbstractClasses
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Animal[] animals =
            {
                new Giraffe{Name = "Geoffrey", Age = 25},
                new Giraffe{Name = "Jaffe", Age = 10},
                new Lion{Name = "Mustafa", Age = 15},
                new Lion{Name = "Simba", Age = 5},
                new Elephant{Name = "Penny", Age = 8},
                new Elephant{Name = "Dumbo", Age = 9,}
            };

            foreach (Animal animal in animals)
            {
                Console.WriteLine("This animal's name is {0}.  It is {1} years old and makes the following sound: ", animal.Name, animal.Age);
                if (animal is IHerbivore)
                {
                    
                    Console.WriteLine(((IHerbivore)animal).Graze());
                    Console.WriteLine(" ");
                }
                else if (animal is ICarnivore)
                {
                    Console.WriteLine(((ICarnivore)animal).Hunt());
                    Console.WriteLine(" ");
                }
            }           
        }
    }
}