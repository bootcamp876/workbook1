﻿using System;

namespace FileProcessing
{
    internal class Program
    {
        static void Main(string[] args)
        {            
            StreamReader? sr = null;
            String fileName = @"C:\Academy2\PayrollData2.txt";

            try
            {
                sr = new StreamReader(fileName);
                while (!sr.EndOfStream)
                {
                    string data = sr.ReadLine();
                    TimeCard tc = TimeCard.CreateTimeCard(data);
                    tc.GetGrossPay();
                    Console.WriteLine(tc.Name + " " + tc.GetGrossPay());
                }               
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("Where's your data?");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            finally
            {
                if (sr != null) sr.Close();
            }
        }
    }
}