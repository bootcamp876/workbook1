﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileProcessing
{
    public class TimeCard
    {
        public string? Name { get; set; }
        public decimal HoursWorked { get; set; }
        public decimal PayRate { get; set; }     

        public static TimeCard CreateTimeCard(string data)
        {
            string[] split=data.Split('|');

            TimeCard card = new TimeCard();
            card.Name = split[0];
            card.HoursWorked = decimal.Parse(split[1]);
            card.PayRate = decimal.Parse(split[2]);

            return card;
        }
        public decimal GetGrossPay()
        {
            decimal pay = 0;
            decimal extraHours = HoursWorked - 40;
            decimal payRate = PayRate;
            decimal overtime = 0;

            decimal result = 0;

            if (HoursWorked > 40)
            {
                pay = HoursWorked * payRate;
                //overtime = extraHours * payRate;
            }

            result = (HoursWorked * PayRate) + overtime;
            return result;                      
        }
    }
}


