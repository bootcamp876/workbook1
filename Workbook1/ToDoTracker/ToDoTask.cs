﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoTracker
{
    public class ToDoTask
    {
        public int TaskId { get; set; }
        public string TaskDescription { get; set; }
        public int Difficulty { get; set; }
        public string AssignedToName { get; set; }
        public string DueDate { get; set; }
    }



}
