﻿using System;

public class Program
{
    public static void Main(string[] args)
    {
        string firstName = "";
        string middleName = "";
        string lastName = "";
        string fullName = "";
        string anotherName = "";

        while (anotherName != "N")
        {
            Console.Write("Enter your Name: ");
            fullName = Console.ReadLine();
            firstName = fullName.Substring(0, fullName.IndexOf(" "));
            middleName = fullName.Substring(fullName.IndexOf(" ") + 1, fullName.LastIndexOf(" ") - fullName.IndexOf(" ") - 1);
            lastName = fullName.Substring(fullName.LastIndexOf(" ") + 1);

            Console.WriteLine(fullName);
            Console.WriteLine(firstName);
            Console.WriteLine(middleName);
            Console.WriteLine(lastName);
        }
    }
}

//fullName.IndexOf(" ") + 1, fullName.LastIndexOf(" ") - fullName.IndexOf(" ")
