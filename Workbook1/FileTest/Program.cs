﻿using System;
using System.IO;

namespace FileTest;

class FileTest
{
    static void Main(string[] args)
    {
        string path = @"C:\Academy";
        string currentFile = @"C:\Academy\Test.txt";
        string newFile = @"C:\Academy\newTest.txt";
        string dateTimeFile = @"C:\Academy\test-backup.txt";

        // Make sure the directory does exist
        if (!Directory.Exists(path))
        {
            Console.WriteLine($"Error: {path} doesn't exist!");
            return;
        }

        string[] fileNames = Directory.GetFiles(path);
        foreach (string name in fileNames)
        {
            Console.WriteLine(name);
        }

        if (!File.Exists(currentFile))
        {
            Console.WriteLine($"Error: {currentFile} doesn't exist!");
            return;
        }
        else
        {
            File.Create(currentFile).Close();
            Console.WriteLine("File created");
        }
               
        if (File.Exists(newFile))
        {
            Console.WriteLine($"Error: {newFile} already exist!");
            return;
        }
       
        // Rename the file
        File.Copy(currentFile, newFile);
        
      
    }
} //class FileTest




