﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Car : Asset
    {
        public int ModelYear { get; set; }
        public int OdometerReading { get; set; }

        public Car(string assetId, string description, string dateAcquired,
             decimal originalCost, int modelYear, int odometerReading) : base(assetId, description, dateAcquired, originalCost)
        {
            this.ModelYear = modelYear;
            this.OdometerReading = odometerReading;
        }

        public override decimal GetValue()
        {
            DateTime now = DateTime.Now;
            int vehicleAge = now.Year - ModelYear;
            decimal depreciate = 0;
            decimal currentValue = 0;

            if (vehicleAge < 7)
            {
                depreciate = .02M;
                currentValue = OriginalCost * (1 - ((OdometerReading / 5000) * .02M));
            }
            else
            {
                if (OdometerReading < 100000)
                {
                    depreciate = .70M;
                }
                else
                {
                    depreciate = .90M;
                }
            }
            return OriginalCost * depreciate;
        }
    }
}
