﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Asset
    {
        public string AssetId { get; set; }
        public string Description { get; set; }
        public string DateAcquired { get; set; }
        public decimal OriginalCost { get; set; }

        public Asset(string assetId, string description, string dateAcquired,
            decimal originalCost)
        {
            this.AssetId = assetId;
            this.Description = description;
            this.DateAcquired = dateAcquired;
            this.OriginalCost = originalCost;
        }

        public virtual decimal GetValue()
        {
            return this.OriginalCost;
        }
    }

}
