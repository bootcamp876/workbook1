﻿using System;

namespace AssetManager
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Asset asset = new Asset("F", "Ford", "01/01/2023", 25);
            Stock stock = new Stock("F", "Ford", "01/01/2023", 25, "F", 25, 500);
            
            Asset[] assets = new Asset[6];
            assets[0] = new Stock("F", "Ford", "01/01/2023", 25, "F", 25, 500);
            assets[1] = new Stock("C", "Chevy", "10/01/2003", 15, "C", 32, 5000);
            assets[2] = new Stock("H", "Honda", "06/01/2015", 10, "H", 12, 1500);
            assets[3] = new Car("F", "Ford", "01/01/2023", 25000, 2010, 210000);
            assets[4] = new Car("C", "Chevy", "10/01/2003", 15000, 2023, 198000);
            assets[5] = new Car("H", "Honda", "06/01/2015", 10000, 2010, 210000);
                     
            DisplayAsset(asset);
            DisplayAsset(stock);
            DisplayAsset(assets[7]);
        }
                
        static void DisplayAsset(Asset asset)
        {
            Console.WriteLine(asset.Description);
            Console.WriteLine(asset.DateAcquired);
            Console.WriteLine(asset.OriginalCost);

            if (asset is Stock)
            {
                
                Stock stock = (Stock)asset;
                Console.WriteLine(stock.StockTicker);
                Console.WriteLine(stock.CurrentSharePrice);
                Console.WriteLine(stock.NumberOfShares);
            }
            else if (asset is Car)
            {
                Car car = (Car)asset;
                Console.WriteLine(car.ModelYear);
                Console.WriteLine(car.OdometerReading);
            }
         }
    }
}

