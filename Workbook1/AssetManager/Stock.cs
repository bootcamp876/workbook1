﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    // The derived class
    public class Stock : Asset
    {
        public string StockTicker { get; set; }
        public int CurrentSharePrice { get; set; }
        public int NumberOfShares { get; set; }

        public Stock(string assetId, string description, string dateAcquired,
            decimal originalCost, string stockTicker, int currentSharePrice, int numberOfShares) : base(assetId, description, dateAcquired, originalCost)
        {
            this.StockTicker = stockTicker;
            this.CurrentSharePrice = currentSharePrice;
            this.NumberOfShares = numberOfShares;
        }

        public override decimal GetValue()
        {
            decimal stockValue = NumberOfShares * CurrentSharePrice;
            return stockValue;         
        }
    }
}
