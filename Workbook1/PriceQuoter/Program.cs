﻿string productCode = "";
int qty = 0;
double perUnitPrice = 0;
double totalPriceOrder;

Console.Write("Enter a Product Code: ");
productCode = Console.ReadLine();

Console.Write("Enter quantity: ");
qty = Convert.ToInt32(Console.ReadLine());

switch (productCode)
{
    case "BG-127":
        {
            if (qty >= 1 && qty <=24)
            {
                perUnitPrice = qty * 18.99;
            } else if (qty >= 125 && qty <= 50)
            {
                perUnitPrice = qty * 17.00;
            } else if (qty >= 51)
            {
                perUnitPrice = qty * 14.49;
            }
            break;
        }
    case "WRTR-28":
        {
            if (qty >= 1 && qty <= 24)
            {
                perUnitPrice = qty * 125.00;
            }
            else if (qty >= 125 && qty <= 50)
            {
                perUnitPrice = qty * 113.75;
            }
            else if (qty >= 51)
            {
                perUnitPrice = qty * 99.99;
            }
            break;
        }
    case "GUAC-8":
        {
            if (qty >= 1 && qty <= 24)
            {
                perUnitPrice = qty * 8.99;
            }
            else if (qty >= 125 && qty <= 50)
            {
                perUnitPrice = qty * 8.99;
            }
            else if (qty >= 51)
            {
                perUnitPrice = qty * 7.49;
            }
            break;
        }
    default:
        {
            Console.WriteLine($"{ productCode}) product not found");
            break;
        }
}

totalPriceOrder = (double)qty * perUnitPrice;
if (totalPriceOrder > 0)
{
    Console.WriteLine($"{productCode} {qty} {perUnitPrice:c} Total price of order {totalPriceOrder:c}");
}