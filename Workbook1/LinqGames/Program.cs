﻿using System;
using System.Linq;
using System.Collections.Generic;
using LinqGames;

namespace LinqGames
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Supplier> suppliers = new List<Supplier>
            {
                new Supplier(101, "ACME", "acme.com"),
                new Supplier(201, "Spring Valley", "spring-valley.com"),
                new Supplier(203, "Costco", "costco.com"),
                new Supplier(205, "Amazon", "amazon.com"),
                new Supplier(405, "Hershey", "hershey.com"),
            };

            List<Product> products = new List<Product>
            {
                new Product(1, "Dark Chocolate Bar", 4.99M, 10, 203),
                new Product(2, "8 oz Guacamole", 5.99M, 27, 203),
                new Product(3, "Milk Chocolate Bar", 3.99M, 16, 405),
                new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 203),
                new Product(5, "Computer Desk", 124.99M, 5, 205),
                new Product(6, "Cleaning Supplies", 5.99M, 20, 101),
                new Product(7, "Office Supplies", 3.99M, 100, 101),
                new Product(8, "Big Screen TV", 1500.99M, 1, 205),
                new Product(9, "Office Plants", 4.99M, 10, 201),
                new Product(10, "Plant food", 5.99M, 5, 201),
            };

            //var supplierQuery = from s in suppliers select s;

            //foreach (Supplier supplier in supplierQuery)
            //{
            //    Console.WriteLine(supplier.Name);
            //}

            var productQuery = from p in products
                               where p.Price <= 5
                               select p;

            foreach (Product product in productQuery)
            {
                Console.WriteLine(product.Price);
            }

            var quantityOnHand = from p in products
                                 where p.QuantityOnHand >= 10
                                 select p;

            //should display in descending order
            foreach (Product product in quantityOnHand)
            {
                Console.WriteLine(product.QuantityOnHand);
            }

            var supplierId = from s in suppliers
                             where s.SupplierId == 201
                             select s;

            foreach (Supplier supplier in supplierId)
            {
                Console.WriteLine($"{supplier.SupplierId} -- {supplier.Name} -- {supplier.URL}");
            }
        } 
    }
}
  
