﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BooksAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Book> Get()
        {
            List<Book> books = new List<Book>();
            SqlConnection cnnStr = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            cnnStr.Open();

            string sql = "Select * From Books";
            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    Book book = new Book();
                    book.BookId = (int)reader["BookId"];
                    book.Title = (string)reader["Title"];
                    book.Author = (string)reader["Author"];
                    book.Publisher = (string)reader["Publisher"];
                    book.YearPublished = (int)reader["YearPublished"];
                    book.Price = (double)reader["Price"];
                    books.Add(book);
                }
            }
            cnnStr.Close();
            return books;
        }

        [HttpGet("{BookId}")]
        public ActionResult<Book> Get(int BookId)
        {
            List<Book> books = new List<Book>();
            SqlConnection cnnStr = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            cnnStr.Open();

            string sql = "Select * From Books";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            SqlParameter sqlParameter = new SqlParameter("BookId", BookId);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    Book book = new Book();
                    book.BookId = (int)reader["BookId"];
                    book.Title = (string)reader["Title"];
                    book.Author = (string)reader["Author"];
                    book.Publisher = (string)reader["Publisher"];
                    book.YearPublished = (int)reader["YearPublished"];
                    book.Price = (double)reader["Price"];
                    books.Add(book);
                }
            }

            Book book2 = books.FirstOrDefault(c => c.BookId == BookId);

            if (book2 == null)
            {
                return NotFound();
            }
            cnnStr.Close();
            return book2;
        }

        [HttpPost]
        public ActionResult Post(Book book)
        // public ActionResult<Book> Post(Book book)
        {
            List<Book> books = new List<Book>();
            SqlConnection cnnStr = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            string query = "INSERT INTO Books(BookId, Title, Author, Publisher, YearPublished, Price) VALUES(" + book.BookId + ",'" + book.Title + "','" + book.Author + "','" + book.Publisher + "'," + book.YearPublished + "," + book.Price + ")";
            cnnStr.Open();

            if (!ModelState.IsValid)
                return BadRequest("Invalid data");

            book.BookId = books.Count + 1;
            books.Add(book);

            SqlCommand cmd = new SqlCommand(query, cnnStr);
            int rowsChanged = cmd.ExecuteNonQuery();

           // string location = $"api/books/{book.BookId}";
            string location = "api/books/bookId";
            return Created(location, book);

            cnnStr.Close();

        }

        [HttpPut("{BookId}")]
        public void Put(Book book, int BookId)
        {
            SqlConnection cnnStr = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            cnnStr.Open();
            SqlCommand cmd = new SqlCommand($"Select count(*) from Books where BookId = {BookId}", cnnStr);
            int result = Convert.ToInt32(cmd.ExecuteScalar());

            List<Book> books = new List<Book>();

            if (result == 0)
            {
                //creates new record in table
                Post(book);
            }
            else
            {
                //updates table
                SqlCommand cmdUpd = new SqlCommand($"Update Books set Title = @Title, Author = @Author, Publisher = @Publisher, YearPublished = @YearPublished, Price = @Price where BookId = @BookId", cnnStr);
                cmdUpd.Parameters.AddWithValue("@BookId", book.BookId);
                cmdUpd.Parameters.AddWithValue("@Title", book.Title);
                cmdUpd.Parameters.AddWithValue("@Author", book.Author);
                cmdUpd.Parameters.AddWithValue("@Publisher", book.Publisher);
                cmdUpd.Parameters.AddWithValue("@YearPublished", book.YearPublished);
                cmdUpd.Parameters.AddWithValue("@Price", book.Price);
                cmdUpd.ExecuteNonQuery();

            }            
            cnnStr.Close();
        }

        [HttpDelete("{BookId}")]
        public ActionResult Delete(Book book, int BookId)
        {
            SqlConnection cnnStr = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            cnnStr.Open();
            SqlCommand cmd = new SqlCommand($"Select count(*) from Books where BookId = {BookId}", cnnStr);
            int result = Convert.ToInt32(cmd.ExecuteScalar());
            
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                SqlCommand cmdDel = new SqlCommand($"Delete from Books where BookId = {BookId}", cnnStr);
                int rowsChanged = cmdDel.ExecuteNonQuery();
               
                return Ok();
            }
            cnnStr.Close();
        }

    }
}

