﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore
{
    public class Cage : InventoryItem
    {
        public string Dimensions { get; set; }
        public string Material { get; set; }

        public Cage(int id, string category, string name, string description, decimal price, int quantity, string dimensions, string material)
            : base(id, category, name, description, price, quantity)
        {
            Dimensions = dimensions;
            Material = material;
        }

        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Description: {Description}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Brand: {Dimensions}");
            Console.WriteLine($"Type: {Material}");           
        }
    }
}
