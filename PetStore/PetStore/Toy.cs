﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore
{
    public class Toy : InventoryItem
    {
        public string Material { get; set; }
        public string RecommendedAge { get; set; }

        public Toy(int id, string category, string name, string description, decimal price, int quantity, string material, string recommendedAge)
            : base(id, category, name, description, price, quantity)
        {
            Material = material;
            RecommendedAge = recommendedAge;
        }

        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Description:{Description}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Brand: {Material}");
            Console.WriteLine($"Type: {RecommendedAge}");
        }
    }
}
