﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore
{
    public class Food : InventoryItem
    {
        public string Brand { get; set; }
        public string FoodType { get; set; }
        public string AnimalType { get; set; }

        public Food(int id, string category, string name, string description, decimal price, int quantity, string brand, string foodType, string animalType) 
            : base(id, category, name, description, price, quantity)
        {
            Brand = brand;
            FoodType = foodType;
            AnimalType = animalType;
        }

        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Description: {Description}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Brand: {Brand}");
            Console.WriteLine($"Food Type: {FoodType}");
            Console.WriteLine($"Animal Type: {AnimalType}");
        }
    }
}
