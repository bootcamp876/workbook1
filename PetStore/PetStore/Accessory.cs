﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore
{
    public class Accessory : InventoryItem
    {
        public string Size { get; set; }
        public string Color { get; set; }

        public Accessory(int id, string category, string name, string description, decimal price, int quantity, string size, string color)
            : base(id, category, name, description, price, quantity)
        {
            Size = size;
            Color = color;
        }

        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Description: {Description}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Brand: {Size}");
            Console.WriteLine($"Type: {Color}");
        }
    }
}
