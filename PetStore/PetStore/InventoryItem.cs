﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore
{
    public abstract class InventoryItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public InventoryItem(int id, string category, string name, string description, decimal price, int quantity)
        {
            Id = id;
            Category = category;
            Name = name;
            Description = description;
            Price = price;
            Quantity = quantity;
        }
        public abstract void DisplayDetails();
        public override string ToString()
        {
            //return $"{Id} - {Name} - {Description} - Category: {Category} ";
            //return($"{Id} {Name}: {Name} - {Description}, {Price:C}");
            return ($"Description: {Description}, Price: {Price:C}");
        }
    }
}
