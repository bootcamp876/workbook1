﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore
{
    public class Aquarium : InventoryItem
    {
        public string Capacity { get; set; }
        public string Shape { get; set; }

        public Aquarium(int id, string category, string name, string description, decimal price, int quantity, string capacity, string shape)
            : base(id, category, name, description, price, quantity)
        {
            Capacity = capacity;
            Shape = shape;
        }

        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Description: {Description}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Brand: {Capacity}");
            Console.WriteLine($"Type: {Shape}");
           
        }
    }
}
