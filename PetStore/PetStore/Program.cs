﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;


namespace PetStore
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<InventoryItem> inventory = ReadFile();
            MainMenu(inventory);
        }

        static void MainMenu(List<InventoryItem> inventoryItems)
        {
            if (inventoryItems.Count > 0)
            {
                foreach (var item in inventoryItems)
                {
                    string productItem = item.ToString();
                    //Console.WriteLine(productItem);
                }

              /*  Console.WriteLine("What do you want to do?");
                Console.WriteLine();
                Console.WriteLine("1 - Find items that match type (derived class name)");
                Console.WriteLine("2 - Find items that fall within a price range");
                Console.WriteLine("3 - Find items that match a keyword in the description");
                Console.WriteLine("4 - Show all items");
                Console.WriteLine("5 - Add an item");
                Console.WriteLine("6 - Quit");
                Console.WriteLine();
                Console.WriteLine("Enter a Selection: ");
              */
                
                 Console.WriteLine("Please select a Category:");
                Console.WriteLine();
                Console.WriteLine("1. Food - Dog Food");
                Console.WriteLine("2. Accessory - Cat Collar");
                Console.WriteLine("3. Cages - Parrot Cage");
                Console.WriteLine("4. Aquarium - Fish Tank");
                Console.WriteLine("5. Toys - Hedgehog Wheel");
             
                
                int selection = Convert.ToInt16(Console.ReadLine());
                Console.WriteLine();

                DisplayDetails(inventoryItems, selection);
               // Console.WriteLine();
            }
        }

        static void DisplayDetails(List<InventoryItem> inventoryItems, int selection)
        {
            InventoryItem inventoryItem = (
                    from item in inventoryItems
                    where item.Id == selection
                    select item).FirstOrDefault();

            inventoryItem.DisplayDetails();

            Console.WriteLine();
            Console.WriteLine("Select an option");
            Console.WriteLine("1 - Purchase Item");
            Console.WriteLine("2 - Return to Main Menu");

            string menuInput = Console.ReadLine();
            Console.WriteLine();

            switch (menuInput)
            {
                case "1":
                    if (inventoryItem != null)
                    {
                       PurchaseItem(inventoryItem);
                    }
                    break;
                case "2":
                    MainMenu(inventoryItems);
                    break;
                default:
                    Console.WriteLine("Invalid input");
                    break;
            }
        }

        public static List<InventoryItem> ReadFile()
        {
            List<InventoryItem> inventoryItems = new List<InventoryItem>(); ;

            string fileName = @"C:\Academy\PetStore.txt";
            StreamReader inputFile = null;
            decimal totalPay = 0;
            string error = null;

            try
            {
                inputFile = new StreamReader(fileName);

                while (!inputFile.EndOfStream)
                {
                    string fileLine = inputFile.ReadLine();
                    string[] values = fileLine.Split(',');
                   // Console.WriteLine(values[1]);
                    error = values[1];

                    if (values.Length > 0)
                    {
                        switch (values[1])
                        {
                            case "Food":
                                Food food = new Food(
                                    Convert.ToInt16(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7],
                                    values[8]
                                );
                                inventoryItems.Add(food);
                                break;
                            case "Accessory":
                                Accessory accessory = new Accessory(
                                    Convert.ToInt16(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7]

                                );
                                inventoryItems.Add(accessory);
                                break;
                            case "Cage":
                                Cage cage = new Cage(
                                    Convert.ToInt16(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7]
                                );
                                inventoryItems.Add(cage);
                                break;
                            case "Aquarium":
                                Aquarium aquarium = new Aquarium(
                                    Convert.ToInt16(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7]

                                );
                                inventoryItems.Add(aquarium);
                                break;
                            case "Toy":
                                Toy toy = new Toy(
                                Convert.ToInt16(values[0]),
                                values[1],
                                values[2],
                                values[3],
                                Convert.ToDecimal(values[4]),
                                Convert.ToInt16(values[5]),
                                values[6],
                                values[7]

                            );
                                inventoryItems.Add(toy);
                                break;
                            default:
                                Console.WriteLine("Couldn't read category from file");
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Program.cs Error opening file: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }
            return inventoryItems;
        }

        public static void PurchaseItem(InventoryItem inventoryItems)
        {
            string invoiceFile = @"C:\Academy\PetStoreInvoice.txt";
            StreamWriter outputFile = new StreamWriter(invoiceFile);

            string name = "";
            string address = "";
            string city = "";
            string state = "";
            string zip = "";
            string phone = "";

            Console.WriteLine();
            Console.WriteLine("Customer Invoice");
            Console.WriteLine(" ");

            Console.WriteLine("Name: ");
            name = Console.ReadLine();
            outputFile.WriteLine("Customer Name: " + name);

            Console.WriteLine("Address: ");
            address = Console.ReadLine();
            outputFile.WriteLine("Address: " + address);

            Console.WriteLine("City: ");
            city = Console.ReadLine();
            outputFile.WriteLine("City: " + city);

            Console.WriteLine("State: ");
            state = Console.ReadLine();
            outputFile.WriteLine("State: " + state);

            Console.WriteLine("Zip: ");
            zip = Console.ReadLine();
            outputFile.WriteLine("Zip: " + zip);

            Console.WriteLine("Phone: ");
            phone = Console.ReadLine();
            outputFile.WriteLine("Phone: " + phone);

            outputFile.WriteLine(" ");

            outputFile.WriteLine("Item(s) purchased: " + inventoryItems);
            outputFile.WriteLine(" ");
            outputFile.Close();
        }
    }
}
