﻿using System;
using System.Collections.Generic;

namespace Superheroes.Models;

public partial class SuperHero
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Nickname { get; set; }

    public string? Superpower { get; set; }

    public string? TelephoneNumber { get; set; }

    public DateTime? DateOfBirth { get; set; }
}
