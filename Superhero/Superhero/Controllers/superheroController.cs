﻿using System;
using Microsoft.AspNetCore.Mvc;
//using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Superheroes;
using Superheroes.Models;
using Microsoft.EntityFrameworkCore;

namespace Superheroes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperheroController : ControllerBase
    {
        private readonly SuperheroContext _context;

        public SuperheroController(SuperheroContext context)
        {
            _context = context;
        }

        // GET: api/superhero
        [HttpGet]
       // [Route("/all")]
        public async Task<IActionResult> GetAll()
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }
            var superheroes = await _context.Superheroes.ToListAsync();
            return Ok(superheroes);
        }
        
        // GET: api/superhero by id
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetById(int Id)
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }
            var superhero = await _context.Superheroes.FindAsync(Id);

            if (superhero == null)
            {
                return NotFound();
            }
            return Ok(superhero);
        }

        // POST: api/superhero
        [HttpPost]
        public async Task<ActionResult<SuperHero>> PostSuperhero(SuperHero superhero)
        {
            if (_context.Superheroes == null)
            {
                return Problem("Entity set 'SuperheroContext.Superhero' is null.");
            }
            _context.Superheroes.Add(superhero);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSuperhero", new { Id = superhero.Id }, superhero);
        }

        // PUT: api/superhero
        [HttpPut("{Id}")]
        public async Task<IActionResult> PutSuperHero(int Id, SuperHero superhero)
        {
            if (Id != superhero.Id)
            {
                return BadRequest();
            }

            _context.Entry(superhero).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperheroExists(Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/superhero/5
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteSuperHero(int Id)
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }

            var superhero = await _context.Superheroes.FindAsync(Id);
            if (superhero == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        [Route("superpower/{superpower}")]
        [HttpGet]
        public ActionResult GetBySuperpower(string superpower)
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }
            var superhero = _context.Superheroes.Where(a => a.Superpower.Contains(superpower)).ToList();
            if(superhero == null)
            {
                return NotFound();
            }

            return Ok(superhero);
        }

        private bool SuperheroExists(int Id)
        {
            return (_context.Superheroes?.Any(e => e.Id == Id)).GetValueOrDefault();
        }
    }
}
