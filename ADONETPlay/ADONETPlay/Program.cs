﻿using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;

namespace ADONETPlay
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false,
                        reloadOnChange: true);

            IConfiguration config = builder.Build();

            string? connStr = config["ConnectionStrings:Northwind"];
            SqlConnection cnnStr = new SqlConnection(connStr);
            cnnStr.Open();

            if (connStr != null)
            {
                Console.WriteLine("Connection Successful");
                Console.WriteLine();
               // Console.WriteLine(connStr);               
            }

           // DisplayCategoryCount(cnnStr);
           // DisplayTotalInventoryValue(cnnStr);
           // AddAShipper(cnnStr);
           // ChangeShipperName(cnnStr);
           // DeleteShipper(cnnStr);

           // DisplayAllProducts(cnnStr);
           // DisplayProductsInCategory(cnnStr);
           // DisplayProductsForSupplier(cnnStr);
           // DisplaySupplierProductCounts(cnnStr);
           // DisplayCustomersandSuppliersByCity(cnnStr);

            DisplayTenMostExpensiveProducts(cnnStr);
            DisplaySalesByCategory(cnnStr);

            cnnStr.Close();
        }

        static void DisplayCategoryCount(SqlConnection cnnStr)
        {
            string sql = "Select Count(*) From Categories";
            SqlCommand cmd = new SqlCommand(sql, cnnStr);

            int categoryCount = (int)cmd.ExecuteScalar();
            Console.WriteLine("Display Category Count");
            Console.WriteLine($"We have  {categoryCount} Categories");
            Console.WriteLine();
        }

        static void DisplayTotalInventoryValue(SqlConnection cnnStr)
        {
            string sql = "Select sum(UnitPrice * UnitsInStock) AS TotalValue FROM Products";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            decimal totalPrice = (decimal)cmd.ExecuteScalar();
            Console.WriteLine("Display Total Inventory Value");
            Console.WriteLine($"The total Inventory value is ${totalPrice}");
            Console.WriteLine();
        }

        static void AddAShipper(SqlConnection cnnStr)
        {
            string CompanyName = "ABC Records";
            string Phone = "313113333";

            string sql = $"Insert Into Shippers(CompanyName, Phone) " +
                $"values ('{CompanyName}', '{Phone}') ";

            Console.WriteLine("Add a Shipper");
            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            int rowsChanged = cmd.ExecuteNonQuery();
            int addShipper = (int)cmd.ExecuteNonQuery();
            Console.WriteLine($"Shipper Added: {rowsChanged} row(s) added");
            Console.WriteLine();
 
        }

        static void ChangeShipperName(SqlConnection cnnStr)
        {
            string sql = "UPDATE Shippers SET CompanyName = 'Motown' " +
                "Where CompanyName = 'ABC Records' ";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine("Change Shipper Name");
            Console.WriteLine($"Shipper Added: {rowsChanged} row(s) updated");
            Console.WriteLine();
        }

        static void DeleteShipper(SqlConnection cnnStr)
        {
            string sql = "DELETE FROM Shippers WHERE CompanyName = 'Motown' ";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            int rowsChanged = cmd.ExecuteNonQuery();

            Console.WriteLine("Shipper Deleted");
            Console.WriteLine($"Shipper Deleted: {rowsChanged} row(s) deleted");
            Console.WriteLine();

        }

        static void DisplayAllProducts(SqlConnection cnnStr)
        {
            string sql = "Select ProductID, ProductName, UnitsInStock, UnitPrice FROM Products";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            SqlDataReader dr = cmd.ExecuteReader();

            Console.WriteLine("DisplayAllProducts");
            Console.WriteLine();

            while (dr.Read())
            {
                int productID = (int)dr["ProductID"];
                string productName = (string)dr["ProductName"];
                Int16 unitsInStock = (Int16)dr["UnitsInStock"];
                decimal unitPrice = (decimal)dr["UnitPrice"];
              
                Console.WriteLine($"{productID} {productName} {unitsInStock} {unitPrice}");                
            }
            Console.WriteLine();
            dr.Close();
        }

        static void DisplayProductsInCategory(SqlConnection cnnStr)
        {
            string categoryName = "Beverages";
            string requestedCategory = "Beverages";

            string sql = "Select ProductID,ProductName,QuantityPerUnit,UnitPrice " +
                "FROM Products JOIN Categories ON Categories.CategoryID=Products.CategoryID " +
                "Where Categories.CategoryName = @catID ";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            cmd.Parameters.AddWithValue("@catID", requestedCategory);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {

                int productID = (int)dr["ProductID"];
                string productName = (string)dr["ProductName"];
                string quantityPerUnit = (string)dr["QuantityPerUnit"];
                decimal unitPrice = (decimal)dr["UnitPrice"];

                Console.WriteLine(
                $"[{productID}] {productName} - {unitPrice:C} - {quantityPerUnit}");
            }
            dr.Close();
        }
        
        static void DisplayProductsForSupplier(SqlConnection cnnStr)
        {
            string supplierName = "Exotic Liquids";
            string requestedCategory = "Exotic Liquids";

            /* 
              string sql = "Select p.ProductID,p.ProductName,p.UnitsInStock,p.UnitPrice,s.CompanyName,c.CategoryName " +
                 "FROM suppliers s " +
                 "INNER JOIN Products p on s.SupplierID = p.SupplierID " +
                 "INNER JOIN Categories c on p.CategoryID = c.CategoryID " +
                 "Where s.CompanyName = '" + supplierName + "';";
             */

            string sql = "Select p.ProductID,p.ProductName,p.UnitsInStock,p.UnitPrice,s.CompanyName,c.CategoryName " +
                "FROM suppliers s " +
                "INNER JOIN Products p on s.SupplierID = p.SupplierID " +
                "INNER JOIN Categories c on p.CategoryID = c.CategoryID " +
                "Where s.CompanyName = @catID ";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            cmd.Parameters.AddWithValue("@catID", requestedCategory);

            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string categoryName = (string)(dr["CategoryName"]);
                int productID = (int)dr["ProductID"];
                string productName = (string)dr["ProductName"];
                Int16 unitsInStock = (Int16)dr["UnitsInStock"];
                decimal unitPrice = (decimal)dr["UnitPrice"];
                
                Console.WriteLine($"Product ID: [{productID}] Product Name: {productName} Quantity: {unitsInStock} Unit Price: {unitPrice:C} Category {categoryName}");
                
            }
            dr.Close();
        }

        static void DisplaySupplierProductCounts(SqlConnection cnnStr)
        {
            string sql = "Select Suppliers.CompanyName, count(Products.SupplierID) AS numberOfItems FROM Products " +
                "JOIN Suppliers ON Products.SupplierID = Suppliers.SupplierID " +
                "Group by Suppliers.CompanyName";

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string companyName = (string)dr["CompanyName"];
                int numItems = (int)dr["numberOfItems"];
              
                Console.WriteLine($"Company Name: {companyName} Number of items {numItems}");              
            }
            dr.Close();
        }

        static void DisplayCustomersandSuppliersByCity(SqlConnection cnnStr)
        {
            string sql = "SELECT City, CompanyName, ContactName " +
                "FROM [Customer and Suppliers by city]";

            /* From Brian
               string sql = "SELECT c.companyname as CompanyName,c.city as City " +
                          "FROM dbo.Customers c " +
                          "union all " +
                          "SELECT s.companyname as CompanyName,s.city as City " +
                          "FROM dbo.suppliers s " +
                          "group by CompanyName,City " +
                          "order by City";
             */

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string companyName = (string)dr["CompanyName"];
                string city = (string)dr["City"];
                string contact = (string)dr["ContactName"];
              
                Console.WriteLine($"Company Name: {companyName} City: {city} Contact: {contact}");                
            }
            dr.Close ();
        }

        static void DisplayTenMostExpensiveProducts(SqlConnection cnnStr)
        {

            SqlCommand cmd = new SqlCommand("Ten Most Expensive Products", cnnStr);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();

            Console.WriteLine("DisplayTenMostExpensiveProducts");
            Console.WriteLine("-------------------------------");

            while (dr.Read())
            {
                string productName =
                (string)dr["TenMostExpensiveProducts"];
                decimal unitPrice = (decimal)dr["UnitPrice"];
                
                Console.WriteLine($"{productName} -- {unitPrice:C}");
            }
            Console.WriteLine();
            dr.Close();

        }

        static void DisplaySalesByCategory(SqlConnection cnnStr)
        {
            string categoryName = "Beverages";
            int year = 1998;

            SqlCommand cmd = new SqlCommand("SalesByCategory", cnnStr);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("CategoryName", categoryName);
            cmd.Parameters.AddWithValue("OrdYear", year);

            Console.WriteLine("DisplaySalesByCategory");
            Console.WriteLine("----------------------");

            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string productName = (string)dr["ProductName"];
                decimal sales = (decimal)dr["TotalPurchase"];

                Console.WriteLine($"{productName} - {sales:C2}");
               
            }
            Console.WriteLine();
            dr.Close();

        }
    }
}

