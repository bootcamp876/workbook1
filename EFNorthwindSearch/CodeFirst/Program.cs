﻿using EFBlockbusters;
using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;

namespace EFNorthwindSearch
{
    public class Program
    {
        static void Main(string[] args)
        {
            DisplayAllCategories();
            string category = Console.ReadLine();

            DisplayMovieCategories(category);           

        }

        static void DisplayAllCategories()
        {
            var context = new BlockbusterMovieEntities();
            var categories = context.Categories;
            Console.WriteLine("What type of movies would you like to see? ");
            Console.WriteLine();

            foreach (var category in categories)
            {
                Console.WriteLine(category.CategoryName + "---" + category.Description);
            }
            
            DisplayCategorySelection(Console.ReadLine());
        }

        static void DisplayMovieCategories(string CategoryName)
        {
            var context = new BlockbusterMovieEntities();

            var categories = context.Categories.FromSql($"GetCategories {CategoryName}").ToList();

            Console.WriteLine("Movie Categories");
            Console.WriteLine();
            Console.WriteLine($"{CategoryName}");

            foreach (var category in categories) 
            {               
                Console.WriteLine(category.CategoryName + "---" + category.Description);
            }
            
            string choice = Console.ReadLine();

            DisplayCategorySelection(choice);
        }

        static void DisplayCategorySelection(string choice)
        {
            var context = new BlockbusterMovieEntities();

            var allMovies = from m in context.Movies
                            join c in context.Categories
                            on m.CategoryID equals c.CategoryId
                            where c.CategoryName == choice
                            select new { m.Title, m.Description, m.Rating };

            Console.WriteLine("{0,-20} {1,-80} {2,-5}", "Title", "Description", "Rating");
            Console.WriteLine("{0,-20} {1,-80} {2,-5}", "-------------", "---------------------------", "-------");

            foreach (var m in allMovies)
                Console.WriteLine($"{m.Title,-20} {m.Description,-80} {m.Rating,-5}");

            Console.WriteLine();    
            DisplayAllCategories();

        }
    }
}