﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class GetMovies : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp =
                @"CREATE PROCEDURE [dbo].[GetMovies]
                @name varchar(50)
             AS
                BEGIN
                SET NOCOUNT ON;
                select * 
                from Movies 
                join Categories on Categories.CategoryId = Movies.CategoryId
                where Categories.CategoryName like @name +'%'
                
             END";
            migrationBuilder.Sql(sp);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
