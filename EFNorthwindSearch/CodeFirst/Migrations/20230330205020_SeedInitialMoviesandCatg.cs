﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class SeedInitialMoviesandCatg : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoryId", "CategoryName", "Description" },
                values: new object[,]
                {
                    { 1, "Thriller", "Lots of scary things happening" },
                    { 2, "Science Fiction", "Out of this world" },
                    { 3, "Horror", "Blood, blood, and more blood" },
                    { 4, "Love Story", "He loves, me.  He love me not" },
                    { 5, "Kids", "Appropriate for children 10 and under" },
                    { 6, "Comedy", "Appropriate for children 10 and under" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "CategoryID", "Description", "Rating", "Title", "Year" },
                values: new object[,]
                {
                    { 1, 1, "An Alien is befriended by a young boy named Elliot", "PG", "ET", 1982 },
                    { 2, 2, "Sharks attack swimmers near the New England tourist town of Amity Island", "PG", "Jaws", 1975 },
                    { 3, 3, "Centered on a disatrous attempt to create a theme park of cloned dinasours", "R", "Jurassic Park", 1993 },
                    { 4, 4, "Epic Civil War drama that focuses on the life of Southern belle Scarlett O'Hara", "G", "Gone With The Wind", 1939 },
                    { 5, 5, "Dr. Henry Frankenstein attempts to create life by assembling a creature from body parts", "G", "Frankenstein", 1956 },
                    { 6, 6, "Based on the crime novel by Amerian Author Mario Puzo", "R", "The Godfather", 1972 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 6);
        }
    }
}
