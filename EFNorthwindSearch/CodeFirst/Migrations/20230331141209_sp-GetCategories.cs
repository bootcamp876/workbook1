﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class spGetCategories : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp =
             @"CREATE PROCEDURE [dbo].[GetCategories]
                @name varchar(50)
             AS
                BEGIN
                SET NOCOUNT ON;
                select * 
                from Categories 
                where CategoryName like @name +'%'
                
             END";

            migrationBuilder.Sql(sp);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
