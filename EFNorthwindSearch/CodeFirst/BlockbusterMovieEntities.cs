﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFBlockbusters.Models;

namespace EFBlockbusters
{
    public class BlockbusterMovieEntities : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BlockbusterMovieDb;Integrated Security=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
            new Category
            {
                CategoryId = 1,
                CategoryName = "Thriller",
                Description = "Lots of scary things happening"
            },
            new Category
            {
                CategoryId = 2,
                CategoryName = "Science Fiction",
                Description = "Out of this world"
            },
            new Category
            {
                CategoryId = 3,
                CategoryName = "Horror",
                Description = "Blood, blood, and more blood"
            },
            new Category
            {
                CategoryId = 4,
                CategoryName = "Love Story",
                Description = "He loves, me.  He love me not"
            },
            new Category
            {
                CategoryId = 5,
                CategoryName = "Kids",
                Description = "Appropriate for children 10 and under"
            },
            new Category
            {
                CategoryId = 6,
                CategoryName = "Comedy",
                Description = "Appropriate for children 10 and under"
            });

            modelBuilder.Entity<Movie>().HasData(
            new Movie
            {
                MovieId = 1,
                Title = "ET",
                Description = "An Alien is befriended by a young boy named Elliot",
                Year = 1982,
                Rating = "PG",
                CategoryID = 1
            },
            new Movie
            {
                MovieId = 2,
                Title = "Jaws",
                Description = "Sharks attack swimmers near the New England tourist town of Amity Island",
                Year = 1975,
                Rating = "PG",
                CategoryID = 2
            },
            new Movie
            {
                MovieId = 3,
                Title = "Jurassic Park",
                Description = "Centered on a disatrous attempt to create a theme park of cloned dinasours",
                Year = 1993,
                Rating = "R",
                CategoryID = 3
            },
            new Movie
            {
                MovieId = 4,
                Title = "Gone With The Wind",
                Description = "Epic Civil War drama that focuses on the life of Southern belle Scarlett O'Hara",
                Year = 1939,
                Rating = "G",
                CategoryID = 4
            },
            new Movie
            {
                MovieId = 5,
                Title = "Frankenstein",
                Description = "Dr. Henry Frankenstein attempts to create life by assembling a creature from body parts", 
                Year = 1956,
                Rating = "G",
                CategoryID = 5
            },
            new Movie
            {
                MovieId = 6,
                Title = "The Godfather",
                Description = "Based on the crime novel by Amerian Author Mario Puzo",
                Rating = "R",
                Year = 1972,
                CategoryID = 6  
            });
        }
    }
}
