﻿using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Data;
using SakilaConsoleApp.Models;

namespace SakilaConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            string selection = "";

            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false,
                        reloadOnChange: true);

            IConfiguration config = builder.Build();

            string? connStr = config["ConnectionStrings:sakila"];
            SqlConnection cnnStr = new SqlConnection(connStr);
            cnnStr.Open();

            if (connStr != null)
            {
                Console.WriteLine("Connection Successful");
                Console.WriteLine();
                // Console.WriteLine(connStr);               
            }

            Console.WriteLine("a. List all the actors");
            Console.WriteLine("b. Add a new actor");
            Console.WriteLine("c. Search actor by first name");
            Console.WriteLine("d. Search actor by film name");
            Console.WriteLine("e. Exit");

            Console.WriteLine();
            Console.WriteLine("Make a Selection from the Menu");
            selection = Console.ReadLine();
            
            if (selection == "a" || selection == "A")
            {
                DisplayAllActors(cnnStr);
            }
            else if (selection == "b" || selection =="B")
            {
                AddAnActor(cnnStr);
            }
           
            DisplayActorCount(cnnStr);
            DisplayAllActors(cnnStr);
            AddAnActor(cnnStr);
          
            cnnStr.Close();
            
        }

        static void DisplayActorCount(SqlConnection cnnStr)
        {
            var context = new SakilaContext();
           // var actorsQuery = from a in context.Actor count a;

            string sql = "Select Count(*) From Actor";
            SqlCommand cmd = new SqlCommand(sql, cnnStr);

            int actorCount = (int)cmd.ExecuteScalar();
            Console.WriteLine("Display Actor Count");
            Console.WriteLine($"We have {actorCount} Actors");
            Console.WriteLine();

          //  foreach (Actor a in actorsQuery)
          //  {
          //      Console.WriteLine($"{a.FirstName} {a.LastName}");
          //  }

        }

        static void DisplayAllActors(SqlConnection cnnStr)
        {
          // var context = new SakilaContext();
          // var ActorsQuery = from a in context.Actor select a;

            string sql = "Select actor_id, first_name, last_name FROM Actor";
            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            SqlDataReader dr = cmd.ExecuteReader();

            Console.WriteLine("Displaying All The Actors");

          //  foreach (Actor a in ActorsQuery)
          //  {
          //      Console.WriteLine($"#{a.ActorId} {a.FirstName} {a.LastName} ");
          //  }
      
            Console.WriteLine();

            while (dr.Read())
            {
                int actorID = (int)dr["actor_id"];
                string firstName = (string)dr["first_name"];
                string lastName = (string)dr["last_name"];

                Console.WriteLine($"{actorID} {firstName} {lastName}");
            }
        
            Console.WriteLine();
            dr.Close();

        }

        static void AddAnActor(SqlConnection cnnStr)
        {

            Console.WriteLine("Add a new Actor");

            Console.WriteLine("Enter the First Name: ");
            string FirstName = Console.ReadLine();

            Console.WriteLine("Enter the Last Name: ");
            string LastName = Console.ReadLine();

           // Console.WriteLine("Enter the Film Name: ");
           // string FilmName = Console.ReadLine();

            string sql = $"Insert Into Actor(FirstName, LastName) " +
                $"values ('{first_name}', '{last_name}') ";

            Console.WriteLine("Adding an Actor");

            SqlCommand cmd = new SqlCommand(sql, cnnStr);
            int rowsChanged = cmd.ExecuteNonQuery();
            int addActor = (int)cmd.ExecuteNonQuery();

            Console.WriteLine($"Actor Added: {rowsChanged} row(s) added");
            Console.WriteLine();

        }

 /*      
                        public class AscendingNameSorter
                        {
                            return (from person in persons
                                    orderby person.FirstName ascending
                                    select new Person
                                    {
                                        Name = person.FirstName
                                    }).ToList();

                            foreach (var person in persons) 
                            {
                                Console.WriteLine($"{person.FirstName} -- {person.LastName}");
                            }
                }



                    public class FilmNameSorter : IComparer<Person>
                    {
                        public int Compare(Person x, Person y)
                        {
                            //Console.WriteLine("DescendingAgeSorter");
                            return y.Age - x.Age;
                        }
                    }

                    public class AscendingNameSorter : IComparer<Person>
                    {
                        public int Compare(Person x, Person y)
                        {
                            //Console.WriteLine("AscendingNameSorter");
                            return x.Name.CompareTo(y.Name);
                        }
                    }

                    public class AscendingStateCitySorter : IComparer<Person>
                    {
                        public int Compare(Person x, Person y)
                        {
                            int compareInt = String.Compare(x.State, y.State, comparisonType: StringComparison.OrdinalIgnoreCase);
                            if (compareInt != 0)
                            {
                                // Console.WriteLine("AscendingStateCitySorter");
                                return String.Compare(x.City, y.City, comparisonType: StringComparison.OrdinalIgnoreCase);
                                //return compareInt;
                            }

                            {
                                //return String.Compare(x.City, y.City, comparisonType: StringComparison.OrdinalIgnoreCase);
                                return compareInt;
                            }
                        }
                    }

                    public class DescendingZipSorter : IComparer<Person>
                    {
                        public int Compare(Person x, Person y)
                        {
                            int per1 = Convert.ToInt32(x.ZipCode);
                            int per2 = Convert.ToInt32(y.ZipCode);
                            return per2 - per1;
                        }
                    }

                        */
    }
}

